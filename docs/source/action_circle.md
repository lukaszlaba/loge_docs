## Circle area

Python code:

------------------------------------------------------

```
import math

#! ### Circle Area calculation

#%img action_circle_fig_1.png

#! For input data

r = 89 #<< - circle radius

Area = math.pi * r**2 #%requ - formula that everyone know

#! So, the area of circle with var_r is val_Area.

```

Loge output report:

------------------------------------------------------

### Circle Area calculation

![Alt text](action_circle_fig_1.png)

For input data

[```r = 89```](id1;None;None)  - circle radius

```Area = pi * r^2  = 24884.5554```  - formula that everyone know

So, the area of circle with  ```r = 89```  is  ```24884.5554``` .

------------------------------------------------------











