# Installing

## Loge requirements

To run Loge the following Python environment must be installed

>1 - Python 3

>2 - Minimal non-standard python dependencies

>> [pyqt5](https://www.riverbankcomputing.com/software/pyqt)

>> [mistune](https://pypi.python.org/pypi/mistune)

>> [pillow](https://pillow.readthedocs.io)

>3 - Optional non-standard python dependencies

>> [unum](https://pypi.python.org/pypi/Unum) - SI unit calculation

>> [matplotlib](http://matplotlib.org) - matplotlib and LaTex display

>> [svgwrite](https://pypi.python.org/pypi/svgwrite) - SVG graphic display

>> [tabulate](https://pypi.python.org/pypi/tabulate) - working with nice looking text table

>> [dxf2svg](https://anastruct.readthedocs.io) - dxf file drawing display

>> [anastruct](https://pypi.python.org/pypi/svgwrite) - 2D structure analysis

## How to run Loge?

Loge is available through PyPI and can be install with pip command. To install Loge with minimal requirements use pip by typing:

```
pip install loge
```

To make all features available install optional dependencies by taping:

```
pip install unum matplotlib svgwrite tabulate dxf2svg anastruct
```

To run Loge use command `loge` from your system command line. Command `python -m loge` works as well.

On Windows system you can also find and run `loge.exe` file in `..\Python3\Scripts` folder. For easy run make shortcut for this file.

If new version of Loge package available upgrade it by typing
```
pip install --upgrade loge
```

## OS compatibility

Windows (10) and Linux (xubuntu) tested.