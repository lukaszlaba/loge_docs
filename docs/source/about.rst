About Loge
==========

What Loge is
------------
Loge is a tool to create interactive report for scientific calculation you made with python script. You can use Loge to create reports ready for publication. Loge use .py file format. All additional Loge's syntax is hidden inside python comments so the python engine does not see it, this is still python standard code file you can execute. Loge comments is based on easy to use Markdown language syntax. Loge has its own code editor and file browser included but you can also use your favorite editor to edit script file and get live report preview when file changed.

Loge is based on Python3 and pyqt5.

Python versions tested 3.11 and 3.12

Loge is continuation of `SeePy <https://bitbucket.org/lukaszlaba/seepy>`_ project. SeePy is based on Python2 and pyqt4. SeePy project is is no longer developed.


Mission
-------
The mission of the project is to provide a simple and practical tool that will interest engineers to use Python language in their daily work.

Stage of development
--------------------
At the moment Loge is available on `PyPI <https://pypi.python.org/pypi/loge>`_ as beta stage software.

Source code
-----------
Source code git repository is available on `Bitbucket <https://bitbucket.org/lukaszlaba/loge>`_.

License
-------
Loge is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

Loge is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Foobar; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (C) 2017-2022, the Loge development team

Development team
----------------

The current co-lead Loge developers:
    - Copyright (C) 2017-2022 Lukasz Laba <lukaszlaba@gmail.com>
    - Copyright (C) 2017 Albert Defler <alnw@interia.eu>