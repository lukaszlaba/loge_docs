import math

#! ### Circle Area calculation

#%img action_circle_fig_1.png

#! For input data

r = 89 #<< - circle radius

Area = math.pi * r**2 #%requ - formula that everyone know

#! So, the area of circle with var_r is val_Area.