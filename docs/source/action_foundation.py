# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

import strupy.units as u
from unum.units import um
u.um = um

import math

#! ### Fundament blokowy po wentylator

show = True #<<< (zobacz podstawy teoretyczne)
if show:
    #%img action_foundation_fig_1.png
    None
    
#! ---

#! ### Dane wejściowe
#! Geometria fundamentu
a_f = 3.8 *  u.m #<< - szerokość fundamentu
b_f = 5.20* u.m #<< - dlugość fundamentu
h_f = 1.4 * u.m #<< - wysokość fundamentu
m_f = a_f * b_f * h_f * 2100 * u.kg / u.m3 #! - masa fundamentu betonowego

#! Dane urzadzenia
m_u = 4640.0 * u.kg #<< - masa urzadzenia
n_m = 991.0 #<< - predkość obrotowa [rpm]
n_m / 60.0 #%requ - częstość w [Hz]
omega_m = n_m / (60.0 * u.s) * 2 * math.pi #! - predkość obrotowa [rad/s]
h_u = 3.2 * u.m #<< - wysokość srodka cęźkośći maszyny wzgl. spodu fund
P_d = 1.57 * u.kN #<< - siła dynamiczna
h_s = 3.2 * u.m #<< - wysokość przyłozenia siły wzgl. spodu fundamentu

#! Podloże gruntowe
C_o = 18.0 * u.MPa / u.m #<< - dynamiczny wsp. podloża (wg. tab.1 PN)

#! ---
#! ### Obliczenia
M_c = m_u + m_f #! - masa calkowita
F = a_f * b_f #! - powierzchnia podstawy fundamentu
p = M_c / F * 10.0 * u.N / u.kg
p = p.asUnit(u.kPa) #! - nacisk statyczny na grunt
#! Współczynniki sztywności podłoża
C_z = C_o * (1 + 2*(a_f + b_f) / (u.m**-1 * F)) * (p/(20*u.kPa))**0.5 #! - dynamiczny współczynnik podłoza
C_x = 0.7 * C_z #! - ddynamiczny współczynnik podłoza
C_fi = C_o * (1 + 2*(a_f + 3 * b_f) / (u.m**-1 * F)) * (p/(20*u.kPa))**0.5 #! - dynamiczny współczynnik podłoża

#! Sztywność podłoża
K_z = C_z * F
K_z = K_z.asUnit(u.kN / u.m) #! - sztywność kier. z

K_x = C_x * F
K_x = K_x.asUnit(u.kN / u.m) #! - sztywność kier. x

I_y = a_f**3 * b_f / 12 #! - moment bezwladności podstawy

K_fi = I_y * C_fi
K_fi = K_fi.asUnit(u.kNm) #! - sztywność na obrót

z_k = (m_f * h_f/2 + m_u * h_u) / M_c #! - wysokość środka cięźkośći układu

#! Połozenie punktu kontroli drgań
zp = 3.2 * u.m #<< wysokość od podstawy fundamentu
xp = 0.0 * u.m #<< odleglość w poziomie od osi fundamentu

delta = 0.1 #<< - błąd szacowania czestotliwości degań własnych
#! ### Kątowa częstość drgań pionowych

Lambda_z = (K_z / M_c)**0.5 #%requ - w [rad/s] (czestość maszyny var_omega_m)
Lambda_zmin = Lambda_z * (1 - delta) #%requ - dolna granica szacowanej czestotliwości
Lambda_zmax = Lambda_z * (1 + delta) #%requ - górna granica szacowanej czestotliwości
#! Szacowana czestość rezonansowa w przedziale val_Lambda_zmin - val_Lambda_zmax

A_oz = P_d / K_z
A_oz =A_oz.asUnit(u.um)#! - przemieszczenie pionowe pod dzialaniem statycznym sily Pd

#! ### Katowa czestość drgan wahadlowych
Lambda_fi = (K_x * K_fi /( M_c * (K_x * z_k**2 + K_fi)) )**0.5 #! - w [rad/s] (czestość maszyny var_omega_m)
Lambda_fimin = Lambda_fi * (1 - delta) #%requ - dolna granica szacowanej czestotliwości
Lambda_fimax = Lambda_fi * (1 + delta) #%requ - górna granica szacowanej czestotliwości
#! Szacowana czestość rezonansowa w przedziale val_Lambda_fimin - val_Lambda_fimax

A_o1 = P_d / K_x * (1 + K_x * h_s * zp / K_fi)
A_o1 = A_o1.asUnit(u.um)#! - przemieszczenie poziome pod dzialaniem statycznym sily Pd

A_o2 = P_d / K_fi * h_s * xp
A_o2 = A_o2.asUnit(u.um)#! - przemieszczenie pionowe pod dzialaniem statycznym sily Pd

#! ### Rodzaj strojenia
#! Ze wzgledu na czestotliwość var_Lambda_z
if  Lambda_zmin < omega_m < Lambda_zmax:
    #! ####!!!Praca w strefie rezonansu var_Lambda_zmin < var_omega_m < var_Lambda_zmax !!!
    None
if  omega_m < Lambda_zmin:
    None
    #! OK - Strojenie wysokie var_omega_m < var_Lambda_zmin 
if  Lambda_zmax < omega_m:
    None
    #! OK - Strojenie niskie var_Lambda_zmax < var_omega_m

#! Ze wzgledu na czestotliwość var_Lambda_fi
if  Lambda_fimin < omega_m < Lambda_fimax:
    #! ####!!!Praca w strefie rezonansu var_Lambda_fimin < var_omega_m < var_Lambda_fimax !!!
    None
if  omega_m < Lambda_fimin:
    None
    #! OK - Strojenie wysokie var_omega_m < var_Lambda_fimin 
if  Lambda_fimax < omega_m:
    None
    #! OK - Strojenie niskie var_Lambda_fimax < var_omega_m
    
show = True #<<< (zobacz wykres strojenia)
if show:
    x_max = 1.2*max(omega_m, Lambda_zmax, Lambda_fimax).asNumber()
    
    y1 = np.array([0, 0, 0.3, 0.3, 0, 0])
    x1 = np.array([0, Lambda_zmin.asNumber(), Lambda_zmin.asNumber(), Lambda_zmax.asNumber(), Lambda_zmax.asNumber(), x_max])
    x1a = np.array([0, Lambda_z.asNumber(), Lambda_z.asNumber(), Lambda_z.asNumber(), Lambda_z.asNumber(), x_max])
    
    y2 = y1
    x2 = np.array([0, Lambda_fimin.asNumber(), Lambda_fimin.asNumber(), Lambda_fimax.asNumber(), Lambda_fimax.asNumber(), x_max])
    x2a = np.array([0, Lambda_fi.asNumber(), Lambda_fi.asNumber(), Lambda_fi.asNumber(), Lambda_fi.asNumber(), x_max])

    xm1 = np.arange(0.0, omega_m.asNumber(), 10.0)
    ym1 = 1.0 * (xm1 / omega_m.asNumber())**2
    ym2 = np.array([0, 0, 1.0, 0, 0])
    xm2 = np.array([0, omega_m.asNumber(), omega_m.asNumber(), omega_m.asNumber(), x_max])
    
    plt.figure(1)
    plt.fill(x1, y1, alpha=0.3, label='szacowany obszar Lambda_z', color='blue')
    plt.plot(x1a, y1, label='Lambda_z', color='blue')
    
    
    plt.fill(x2, y2, alpha=0.3, label='szacowany obszar Lambda_fi', color='green')
    plt.plot(x2a, y2, label='Lambda_fi', color='green')
    
    
    plt.plot(xm1, ym1, label='sila wymuszajaca', color='yellow')
    plt.plot(xm2, ym2, label='omega_m', color='red')
    plt.legend()
    plt #%plt
    #plt.show()
    plt.clf()

#! ### Naprezenia pod fundamentem
p #! - nacisk statyczny na grunt od fundamentu i maszyny
sigma_pov = P_d / F
sigma_pov = sigma_pov.asUnit(u.kPa)#! - statyczny nacisk od sily wymuszajacej pionowo

sigma_poh = (P_d * h_s)/(a_f**2 * b_f / 6)
sigma_poh = sigma_poh.asUnit(u.kPa)#! - statyczny nacisk od sily wymuszajacej poziomo


#! ### Amplitudy drgań

gamma = 0.13 #! - tłumienie gruntu

#!--------------------------------------------------------------------------------
#! ###Praca na pełnych obrotach val_omega_m
#! Dopuśćzalne amplitudy drgań dla val_omega_m
omega_m / (2*math.pi) * 60.0 #%requ x [obr]
show = True #<<< (zobacz wykres PN)
if show:
    #%img action_foundation_fig_3.png
    None
A_xdop = 110.0 * u.um    #<< - poziomych 
A_zdop = 70.0 * u.um   #<< - pionowych
#! #### Drgania pionowe
if omega_m < Lambda_zmin:
    Lambda_zmod = Lambda_zmin #%requ strojenie wysokie
if omega_m > Lambda_zmax:
    Lambda_zmod = Lambda_zmax #%requ strojenie niskie
if Lambda_zmin <= omega_m <= Lambda_zmax:
    Lambda_zmod = omega_m #%requ praca w strefie rezonansu!!!
    Lambda_zmod = Lambda_zmod * 0.999
    
eta_z = omega_m / Lambda_zmod #%requ

tlumienie = False
if 0.75<eta_z<1.25:
    #! fundament pracuje w strefie rezonanasu
    tlumienie = True #<<< - dodatkwowo uzglednic tłumienie var_gamma (nie zalecane)
    
if tlumienie:
    ni = 1 / ((1 - eta_z**2)**2 +  gamma**2)**0.5 #%requ - wzmocnienie
else:
    ni = 1 / ((1 - eta_z**2)**2)**0.5 #%requ - wzmocnienie
    
A_dz = ni * A_oz   #%requ - Amplituda drgań pionowych

#! #### Drgania wahadłowe
if omega_m < Lambda_fimin:
    Lambda_fimod = Lambda_fimin #%requ strojenie wysokie
if omega_m > Lambda_fimax:
    Lambda_fimod = Lambda_fimax #%requ strojenie niskie
if Lambda_fimin <= omega_m <= Lambda_fimax:
    Lambda_fimod = omega_m #%requ praca w strefie rezonansu!!!
    Lambda_fimod = Lambda_fimod * 0.999
    
eta_fi = omega_m / Lambda_fimod #%requ

tlumienie = False
if 0.75<eta_fi<1.25:
    #! fundament pracuje w strefie rezonanasu
    tlumienie = False #<<< - dodatkwowo uzglednic tłumienie var_gamma (nie zalecane)
    
if tlumienie:
    ni = 1 / ((1 - eta_fi**2)**2 +  gamma**2)**0.5 #%requ - wzmocnienie
else:
    ni = 1 / ((1 - eta_fi**2)**2)**0.5 #%requ - wzmocnienie
A_do1 = ni * A_o1   #%requ - amplituda drgań poziomych
A_do2 = ni * A_o2   #%requ - amplituda drgań pionowych
#! #### Drgania całkowite
A_x = A_do1 #%requ - całkowita amplituda drgań poziomych var_A_xdop
A_z = A_dz + A_do2 #%requ - całkowita amplituda drgań pionowych var_A_zdop

if (A_x > A_xdop) or (A_z > A_zdop):
    None
    #! ### !! Przekroczone amplitudy drgań !!

if Lambda_zmax < omega_m:
    #!--------------------------------------------------------------------------------
    #! ### Rezonans przejśćiowy dla var_Lambda_zmax (drgania pionowe)
    omega_r = Lambda_zmax #!
    omega_r / (2*math.pi) * 60.0 #%requ x [obr]
    #! Dopuśćzalne amplitudy drgań dla val_omega_r
    show = True #<<< (zobacz wykres PN)
    if show:
        #%img action_foundation_fig_3.png
        None
    A_xdop = 120.0 * u.um #<< - poziomych 
    A_zdop = 80.0 * u.um #<< - pionowych

    P_dred = P_d * (omega_r / omega_m)**2 #%requ -siła dynamiczna dla obrotów val_omega_r
    #! Drgania pionowe
    eta_z = omega_r / (Lambda_z) #%requ
    ni = 1 / ((1 - eta_z**2)**2 +  gamma**2)**0.5 #%requ - wzmocnienie
    A_dz = P_dred / P_d * ni * A_oz   #%requ - Amplituda drgań pionowych

    #! Drgania wahadłowe
    eta_fi = omega_r / (1.25* Lambda_fi) #%requ
    ni = 1 / ((1 - eta_fi**2)**2 +  gamma**2)**0.5 #%requ - wzmocnienie
    A_do1 = P_dred / P_d * ni * A_o1   #%requ - amplituda drgań poziomych
    A_do2 = P_dred / P_d * ni * A_o2   #%requ - amplituda drgań pionowych
    #! Drgania całkowite
    A_x = A_do1 #%requ - całkowita amplituda drgań poziomych var_A_xdop
    A_z = A_dz + A_do2 #%requ - całkowita amplituda drgań pionowych var_A_zdop

    if (A_x > A_xdop) or (A_z > A_zdop):
        None
        #! ### !! Przekroczone amplitudy drgań !!

if Lambda_fimax < omega_m:
    #!--------------------------------------------------------------------------------
    #! ### Rezonans przejśćiowy dla Lambda_fimax (drgania wahadłowe)
    omega_r = Lambda_fi #!
    omega_r / (2*math.pi) * 60.0 #%requ x [obr]
    #! Dopuśćzalne amplitudy drgań dla val_omega_r
    show = True #<<< (zobacz wykres PN)
    if show:
        #%img action_foundation_fig_3.png
        None
    A_xdop = 140.0 * u.um  #<< - poziomych
    A_zdop = 100.0 * u.um  #<< - pionowych

    P_dred = P_d * (omega_r / omega_m)**2 #%requ -siła dynamiczna dla obrotów val_omega_r
    #! Drgania pionowe
    eta_z = omega_r / (0.75 * Lambda_z) #%requ
    ni = 1 / ((1 - eta_z**2)**2 +  gamma**2)**0.5 #%requ - wzmocnienie
    A_dz = P_dred / P_d * ni * A_oz   #%requ - Amplituda drgań pionowych

    #! Drgania wahadłowe
    eta_fi = omega_r / (Lambda_fi) #%requ
    ni = 1 / ((1 - eta_fi**2)**2 +  gamma**2)**0.5 #%requ - wzmocnienie
    A_do1 = P_dred / P_d * ni * A_o1   #%requ - amplituda drgań poziomych
    A_do2 = P_dred / P_d * ni * A_o2   #%requ - amplituda drgań pionowych
    #! Drgania całkowite
    A_x = A_do1 #%requ - całkowita amplituda drgań poziomych var_A_xdop
    A_z = A_dz + A_do2 #%requ - całkowita amplituda drgań pionowych var_A_zdop

    if (A_x > A_xdop) or (A_z > A_zdop):
        None
        #! ### !! Przekroczone amplitudy drgań !!

#! ---
show = True #<<< (pomocnicze - zobacz wykres wsp. dynamicznego)
if show:
    #%img action_foundation_fig_2.png
    None