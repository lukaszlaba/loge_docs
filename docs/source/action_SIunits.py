import unum.units as u

#! ### Stress calculation

#! #### Input data:
F = 1000.0*u.N #<< - force value
b = 20.0 * u.cm #<< - section width
h = 10.0 * u.cm #<< - section height

#! #### Result calculation:
A = b * h #%requ - section area
Sigma = (F / A).asUnit(u.Pa) #%requ - stress value

#! So for section dimensions var_b , var_h and force var_F we get stress value val_Sigma.

