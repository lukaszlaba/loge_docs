Loge python notebook
--------------------

Easy and fast dynamic report generation with Python 3

.. figure:: loge1.png

.. raw:: html

    <iframe width="700" height="315"
    src="https://www.youtube.com/embed/BYR5CaOhuM4"
    frameborder="0" allowfullscreen></iframe>

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   about
   install
   syntax
   action

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`