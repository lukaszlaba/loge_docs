## SI unit calculation

SI unit calculation can by made with [Unum](https://pypi.python.org/pypi/Unum) pagkage.
[Here](http://home.scarlet.be/be052320/Unum.html) you can find Unum documentation. Please note that Loge has some special features that help diplay Unum - you can find it in Features page.


Python code:

------------------------------------------------------

```
import unum.units as u

#! Stress calculation

#! Input data:
F = 1000.0*u.N #<< - force value
b = 20.0 * u.cm #<< - section width
h = 10.0 * u.cm #<< - section height

#! Result calculation:
A = b * h #%requ - section area
Sigma = (F / A).asUnit(u.Pa) #%requ - stress value

#! So for section dimensions var_b , var_h and force var_F we get stress value val_Sigma.

```

Loge output report:

------------------------------------------------------

Stress calculation

Input data:

[```F = 1000.0 [N]```](id1;None;None)  - force value

[```b = 20.0 [cm]```](id2;None;None)  - section width

[```h = 10.0 [cm]```](id3;None;None)  - section height

Result calculation:

```A = b * h  = 200.0 [cm2]```  - section area

```Sigma = F / A  = 50000.0 [Pa]```  - stress value

So for section dimensions  ```b = 20.0 [cm]```  ,  ```h = 10.0 [cm]```  and force  ```F = 1000.0 [N]```  we get stress value  ```50000.0 [Pa]``` .

------------------------------------------------------